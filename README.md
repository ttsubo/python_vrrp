# python_vrrp

This tool aims to confirm how VRRP protocol works

## Requirement

The following two softwares must be installed in advance.

- Docker
- docker-compose

## How to Run

First of all, it needs to prepare Docker Imager for handling vrrp implementation on your environment

    docker-compose build

And then, you can run ...

    $ docker-compose up -d
    Creating vrrp2 ... done
    Creating vrrp1 ... done

## How to Confirm behavior of vrrp processing

You can observe logs of vrrp processing as following

    $ docker-compose logs -f vrrp1
    Attaching to vrrp1
    vrrp1    | 2020-05-12 06:35:08,978:INFO:app_manager.py:225:load_apps:loading app sample_vrrp1.py
    vrrp1    | 2020-05-12 06:35:08,980:INFO:app_manager.py:225:load_apps:loading app python_vrrp.base.manager
    vrrp1    | 2020-05-12 06:35:08,989:INFO:app_manager.py:305:_instantiate:instantiating app sample_vrrp1.py of SampleVrrp
    vrrp1    | 2020-05-12 06:35:08,989:INFO:app_manager.py:305:_instantiate:instantiating app python_vrrp.base.manager of VRRPManager
    vrrp1    | 2020-05-12 06:35:08,990:INFO:sample_vrrp1.py:31:_start:
    vrrp1    | 2020-05-12 06:35:08,991:INFO:sample_vrrp1.py:32:_start:////// 1. Create Vrrp Router  //////
    vrrp1    | 2020-05-12 06:35:08,991:INFO:app_manager.py:305:_instantiate:instantiating app None of VRRPInterfaceMonitorNetworkDevice
    vrrp1    | 2020-05-12 06:35:09,003:INFO:app_manager.py:305:_instantiate:instantiating app None of VRRPRouterV3
    vrrp1    | 2020-05-12 06:35:09,004:INFO:sample_vrrp1.py:91:vrrp_state_changed_handler:State Changed [None] -> [Initialize]
    vrrp1    | 2020-05-12 06:35:09,004:INFO:sample_vrrp1.py:91:vrrp_state_changed_handler:State Changed [Initialize] -> [Backup]
    vrrp1    | 2020-05-12 06:35:19,965:WARNING:router.py:604:preempt_delay:VRRPV3StateBackup preempt_delay
    vrrp1    | 2020-05-12 06:35:19,966:INFO:sample_vrrp1.py:91:vrrp_state_changed_handler:State Changed [Backup] -> [Master]
    vrrp1    | 2020-05-12 06:35:38,986:INFO:sample_vrrp1.py:42:_start:
    vrrp1    | 2020-05-12 06:35:38,986:INFO:sample_vrrp1.py:43:_start:////// 2. Change Priority [250] -> [50]  //////
    vrrp1    | 2020-05-12 06:35:38,987:WARNING:router.py:549:vrrp_config_change_request:VRRPV3StateMaster vrrp_config_change_request
    vrrp1    | 2020-05-12 06:35:48,996:INFO:sample_vrrp1.py:91:vrrp_state_changed_handler:State Changed [Master] -> [Backup]
    vrrp1    | 2020-05-12 06:36:08,965:INFO:sample_vrrp1.py:47:_start:
    vrrp1    | 2020-05-12 06:36:08,966:INFO:sample_vrrp1.py:48:_start:////// 3. Change Priority [50] -> [250]  //////
    vrrp1    | 2020-05-12 06:36:08,966:WARNING:router.py:640:vrrp_config_change_request:VRRPV3StateBackup vrrp_config_change_request
    vrrp1    | 2020-05-12 06:36:19,029:WARNING:router.py:604:preempt_delay:VRRPV3StateBackup preempt_delay
    vrrp1    | 2020-05-12 06:36:19,030:INFO:sample_vrrp1.py:91:vrrp_state_changed_handler:State Changed [Backup] -> [Master]
    vrrp1    | 2020-05-12 06:36:38,946:INFO:sample_vrrp1.py:52:_start:
    vrrp1    | 2020-05-12 06:36:38,946:INFO:sample_vrrp1.py:53:_start:////// 4. Shutdown Vrrp Router  //////
    vrrp1    | 2020-05-12 06:36:38,948:INFO:sample_vrrp1.py:91:vrrp_state_changed_handler:State Changed [Master] -> [Initialize]

    $ docker-compose logs -f vrrp2
    Attaching to vrrp2
    vrrp2    | 2020-05-12 06:34:39,279:INFO:app_manager.py:225:load_apps:loading app sample_vrrp2.py
    vrrp2    | 2020-05-12 06:34:39,281:INFO:app_manager.py:225:load_apps:loading app python_vrrp.base.manager
    vrrp2    | 2020-05-12 06:34:39,289:INFO:app_manager.py:305:_instantiate:instantiating app sample_vrrp2.py of SampleVrrp
    vrrp2    | 2020-05-12 06:34:39,290:INFO:app_manager.py:305:_instantiate:instantiating app python_vrrp.base.manager of VRRPManager
    vrrp2    | 2020-05-12 06:34:39,291:INFO:sample_vrrp2.py:31:_start:
    vrrp2    | 2020-05-12 06:34:39,291:INFO:sample_vrrp2.py:32:_start:////// 1. Create Vrrp Router  //////
    vrrp2    | 2020-05-12 06:34:39,291:INFO:app_manager.py:305:_instantiate:instantiating app None of VRRPInterfaceMonitorNetworkDevice
    vrrp2    | 2020-05-12 06:34:39,302:INFO:app_manager.py:305:_instantiate:instantiating app None of VRRPRouterV3
    vrrp2    | 2020-05-12 06:34:39,303:INFO:sample_vrrp2.py:58:vrrp_state_changed_handler:State Changed [None] -> [Initialize]
    vrrp2    | 2020-05-12 06:34:39,304:INFO:sample_vrrp2.py:58:vrrp_state_changed_handler:State Changed [Initialize] -> [Backup]
    vrrp2    | 2020-05-12 06:34:42,919:INFO:sample_vrrp2.py:58:vrrp_state_changed_handler:State Changed [Backup] -> [Master]
    vrrp2    | 2020-05-12 06:35:19,968:INFO:sample_vrrp2.py:58:vrrp_state_changed_handler:State Changed [Master] -> [Backup]
    vrrp2    | 2020-05-12 06:35:48,993:WARNING:router.py:604:preempt_delay:VRRPV3StateBackup preempt_delay
    vrrp2    | 2020-05-12 06:35:48,995:INFO:sample_vrrp2.py:58:vrrp_state_changed_handler:State Changed [Backup] -> [Master]
    vrrp2    | 2020-05-12 06:36:19,032:INFO:sample_vrrp2.py:58:vrrp_state_changed_handler:State Changed [Master] -> [Backup]
    vrrp2    | 2020-05-12 06:36:39,560:INFO:sample_vrrp2.py:58:vrrp_state_changed_handler:State Changed [Backup] -> [Master]
