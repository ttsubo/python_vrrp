import logging
import time
import eventlet
from ryu.lib import hub
from ryu.lib.rpc import RPCError
from python_vrrp.base import api as vrrp_api
from python_vrrp.base import event as vrrp_event
from python_vrrp.base import handler
from python_vrrp.app_manager import VrrpApp, AppManager
eventlet.monkey_patch()

LOG = logging.getLogger('SampleVrrp')
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(filename)s:%(lineno)s:%(funcName)s:%(message)s',
                    level=logging.INFO)

_VRRP_VERSION_V3 = 3
_VRID = 1
_VIRTUAL_IP_ADDRESS = '192.168.0.1'
_PRIMARY_IP_ADDRESS = '192.168.0.3'
_VIRTUAL_MAC_ADDRESS = '00:00:5e:00:01:01'
_PRIORITY = 100
_IFNAME = 'eth0'
_PREEMPT_DELAY = 10

class SampleVrrp(VrrpApp):
    def __init__(self, *args, **kwargs):
        super(SampleVrrp, self).__init__(*args, **kwargs)
        self.vrrp_thread = hub.spawn(self._start)

    def _start(self):
        LOG.info("")
        LOG.info("////// 1. Create Vrrp Router  //////")
        self._configure_vrrp_router(_VRRP_VERSION_V3,
                                    _PRIORITY,
                                    _PRIMARY_IP_ADDRESS,
                                    _VIRTUAL_IP_ADDRESS,
                                    _IFNAME,
                                    _VRID,
                                    _PREEMPT_DELAY)
    
    def _configure_vrrp_router(self, vrrp_version, vrrp_priority,
                               primary_ip_address, virtual_ip_address,
                               ifname, vrid, preempt_delay):
        interface = vrrp_event.VRRPInterfaceNetworkDevice(
            _VIRTUAL_MAC_ADDRESS, primary_ip_address, None, ifname)

        ip_addresses = [virtual_ip_address]
        config = vrrp_event.VRRPConfig(
            version=vrrp_version, vrid=vrid, priority=vrrp_priority,
            ip_addresses=ip_addresses, preempt_delay=preempt_delay)
        config_result = vrrp_api.vrrp_config(self, interface, config)
        return config_result

    @handler.set_ev_cls(vrrp_event.EventVRRPStateChanged)
    def vrrp_state_changed_handler(self, ev):
        old_state = ev.old_state
        new_state = ev.new_state
        LOG.info("State Changed [%s] -> [%s]"% (old_state, new_state))

if __name__ == '__main__':
    app_lists = ['sample_vrrp2.py']
    app_mgr = AppManager.get_instance()
    app_mgr.load_apps(app_lists)
    contexts = app_mgr.create_contexts()
    services = []
    services.extend(app_mgr.instantiate_apps(**contexts))
    try:
        hub.joinall(services)
    except KeyboardInterrupt:
        LOG.info("Keyboard Interrupt received. "
                     "Closing VRRP application manager...")
    finally:
        app_mgr.close()