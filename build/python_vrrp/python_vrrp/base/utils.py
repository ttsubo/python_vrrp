from ryu.lib.packet import ethernet
from ryu.lib.packet import vlan
from ryu.lib.packet.ether_types import *


def may_add_vlan(packet, vlan_id):
    """
    :type packet: ryu.lib.packet.packet.Packet
    :param packet:
    :type vlan_id: int (0 <= vlan_id <= 4095) or None (= No VLAN)
    :param vlan_id:
    """
    if vlan_id is None:
        return

    e = packet.protocols[0]
    assert isinstance(e, ethernet.ethernet)
    v = vlan.vlan(0, 0, vlan_id, e.ethertype)
    e.ethertype = ETH_TYPE_8021Q
    packet.add_protocol(v)



def dp_packet_out(dp, port_no, data):
    # OF 1.2
    ofproto = dp.ofproto
    ofproto_parser = dp.ofproto_parser
    actions = [ofproto_parser.OFPActionOutput(port_no,
                                              ofproto.OFPCML_NO_BUFFER)]
    packet_out = ofproto_parser.OFPPacketOut(
        dp, 0xffffffff, ofproto.OFPP_CONTROLLER, actions, data)
    dp.send_msg(packet_out)


def dp_flow_mod(dp, table, command, priority, match, instructions,
                out_port=None):
    # OF 1.2
    ofproto = dp.ofproto
    ofproto_parser = dp.ofproto_parser
    if out_port is None:
        out_port = ofproto.OFPP_ANY
    flow_mod = ofproto_parser.OFPFlowMod(
        dp, 0, 0, table, command, 0, 0,
        priority, 0xffffffff, out_port, ofproto.OFPG_ANY,
        ofproto.OFPFF_CHECK_OVERLAP, match, instructions)
    dp.send_msg(flow_mod)
